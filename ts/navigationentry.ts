import * as plugins from './navigation.plugins';

export class NavigationEntry {
  level: number;
  group: string;
  groupRanking: number;
  iconUrl: string;
  id: string;
  name: string;
  callBack: (idArg?: string) => void;
}