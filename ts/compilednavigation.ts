import * as plugins from './navigation.plugins';
import { NavigationManager } from './navigationmanager';

export class CompiledNavigation {
  /**
   * 
   */
  public static fromNavigationManager(navigationManagerArg: NavigationManager) {
    const compiledNavInstance = new CompiledNavigation();
    return compiledNavInstance;
  }
}