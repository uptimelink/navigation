import { CompiledNavigation } from './compilednavigation';
import * as plugins from './navigation.plugins';
import { NavigationEntry } from './navigationentry';

export class NavigationManager {
  navigationManagers: NavigationManager[] = [];
  navigationEntries: NavigationEntry[] = [];
  
  public compiledNavigationObservable = new plugins.smartrx.rxjs.Subject<CompiledNavigation>();

  public addNavigationManager(navManagerArg: NavigationManager) {
    this.navigationManagers.push(navManagerArg);
    this.compile();
  }
  
  public addNavigationEntry(navEntryArg: NavigationEntry) {
    this.navigationEntries.push(navEntryArg);
    this.compile();
  }

  
  public async compile(): Promise<CompiledNavigation> {
    const compiledNavigationInstance = CompiledNavigation.fromNavigationManager(this);
    this.compiledNavigationObservable.next(compiledNavigationInstance);
    return compiledNavigationInstance;
  }
}